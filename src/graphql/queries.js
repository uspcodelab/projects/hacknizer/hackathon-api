const queries = `
  type Query {
    hackathon(id: ID!): Hackathon!

    """Queries Hackathons by a participant id."""
    hackathonsByUser(id: ID!): [Hackathon!]

    """Queries Hackathons within a time frame."""
    hackathonsByDate(start: String!, end: String!): [Hackathon!]

    """Queries for $count hackathons after a given start date."""
    hackathonsAfterDate(start: DateInput!, count: Int!): [Hackathon!]
  }
    
`;

export default queries;
