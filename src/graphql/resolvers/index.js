import hackathonEditionQueries from "./queries/next_hackathons";

import hackathonMutations from "./mutations/hackathon";
import hackathonEditionMutations from "./mutations/hackathon_edition";

export default {
  Query: {
    ...hackathonEditionQueries
  },
  Mutation: {
    ...hackathonMutations,
    ...hackathonEditionMutations
  }
}
