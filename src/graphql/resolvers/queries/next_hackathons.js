import { formatDateArray, formatDate } from '../../utils'
import { ApolloError } from 'apollo-server-koa'
import { Op } from 'sequelize'


export default {
  async hackathon(parent, args, context, info) {
    const { Hackathon, HackathonEdition } = context.db;

    const result = await Hackathon.findById(args.id);
  
    if (!result)
      throw new ApolloError("Hackathon not found",400);

    let hackathon = {...result.dataValues};

    hackathon.editions =
      await HackathonEdition.findAll({where: {hackathonId: hackathon.id}});

    hackathon.editions = hackathon.editions.map(edition => edition.dataValues)
    formatDateArray(hackathon.editions)

    return hackathon;
  },

  async hackathonsAfterDate(parent, args, context, info) {
    const { HackathonEdition } = context.db;

    const { day, month, year } = args.start;
    const date = new Date(`${year}/${month}/${day}`);

    let query_editions =
      await HackathonEdition.findAll({
        where: {
          date_start: {
            [Op.gte]: date,
            [Op.ne]: null
          },
          date_end: {
            [Op.ne]: null
          }
        },
        order: [['date_start', 'ASC']],
        limit: args.count
      });

    let editions = query_editions.map(edition => edition.dataValues);

    let hackathons = await Promise.all(
      editions.map(
        async edition => {
          let hackathon = await Hackathon.findById(edition.hackathonId);
          hackathon = hackathon.dataValues;
          hackathon.editions = [edition];
          formatDateArray(hackathon.editions)
          return hackathon;
        }
      )
    );

    return hackathons;
  }
};
