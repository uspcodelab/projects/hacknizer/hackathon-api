/*
  Param edition: the dataValues of a hackathon edition
*/
export function formatDate(edition) {
  ['registration_start', 'registration_end', 'date_start', 'date_end'].forEach(
      date => {
        const currDate = edition[date];
        if (currDate)
          edition[date] = {
            day: currDate.getDate(),
            month: currDate.getMonth() + 1,
            year: currDate.getFullYear()
          };
      });
}

export function formatDateArray(editions) {
  editions.forEach(edition => formatDate(edition));
}