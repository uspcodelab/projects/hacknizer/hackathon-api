import Cors from '@koa/cors';
import {ApolloServer} from 'apollo-server-koa';
import Koa from 'koa';
import nats from 'nats';

import db from './db/models';
import {resolvers, typeDefs} from './graphql/schema.js';

const PORT = process.env.PORT || 3000;

let nc;

if (process.env.NATS_PRESENT != 'false') {
  nc = nats.connect({url: process.env.NATS_URL, preserveBuffers: true});
}

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ctx}) => ({ctx: ctx, db: db, nats: nc ? nc : null})
});

const app = new Koa();
app.use(Cors());

server.applyMiddleware({app});

app.listen(
    {port: PORT},
    () => console.log(
        `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`),
);

export default app;
