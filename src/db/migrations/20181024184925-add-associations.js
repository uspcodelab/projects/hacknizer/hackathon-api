'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'Hackathons',
      'ownerId',
      {
        type: Sequelize.STRING,
        references: {
          model: 'Users',
          key: 'id'
        },
      }
    );

    await queryInterface.addColumn(
      'HackathonEditions',
      'hackathonId',
      {
        type: Sequelize.STRING,
        references: {
          model: 'Hackathons',
          key: 'id'
        }
      }
    );

    return queryInterface.addColumn(
      'Prizes',
      'hackathonEditionId',
      {
        type: Sequelize.STRING,
        references: {
          model: 'HackathonEditions',
          key: 'id'
        }
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'Hackathons',
      'ownerId'
    );

    await queryInterface.removeColumn(
      'HackathonEditions',
      'hackathonId'
    );

    return queryInterface.removeColumn(
      'Prizes',
      'HackathonEditionId'
    );
  }
};
