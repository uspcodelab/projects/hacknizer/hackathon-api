'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    }
  },
  {
    indexes: [
      { unique: true, fields: ['id'] }
    ]
  });

  User.associate = function(models) {
    User.hasMany(models.Hackathon, { foreignKey: 'ownerId' })
  };

  return User;
};
