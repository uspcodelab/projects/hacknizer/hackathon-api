'use strict';

module.exports = (sequelize, DataTypes) => {
  const Hackathon = sequelize.define('Hackathon', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      unique: true
    }
  },
  {
    indexes: [
      { unique: true, fields: ['id'] }
    ]
  });

  Hackathon.associate = function (models) {
    Hackathon.belongsTo(models.User, { foreignKey: 'ownerId' })
    Hackathon.hasMany(models.HackathonEdition, { foreignKey: 'hackathonId' })
  };

  return Hackathon;
};
